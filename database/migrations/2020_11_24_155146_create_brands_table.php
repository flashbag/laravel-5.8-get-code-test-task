<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->unique();

            $table->timestamps();
        });

        Schema::table('items', function (Blueprint $table) {

            $table->unsignedBigInteger('brand_id')->nullable()->after('ticket_id');
            $table->foreign('brand_id')->references('id')->on('brands');

            $table->unsignedBigInteger('category_id')->nullable()->after('brand_id');
            $table->foreign('category_id')->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {

            $table->dropForeign(['category_id']);
            $table->dropColumn('category_id');

            $table->dropForeign(['brand_id']);
            $table->dropColumn('brand_id');

        });

        Schema::dropIfExists('brands');
    }
}
