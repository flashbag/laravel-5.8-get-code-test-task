<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Http\Requests\Tickets\StoreTicketRequest;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::paginate(20);

        return view('tickets.index', [
            'items' => $tickets,
            'count' => $tickets->total()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTicketRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTicketRequest $request)
    {
        $validated = $request->validated();

        $ticket = Ticket::create($validated);

        return response()
            ->json(['redirect' => '/tickets/' . $ticket->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket  $ticket)
    {
        return view('tickets.show', [
            'ticket' => $ticket
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
