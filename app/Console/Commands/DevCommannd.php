<?php

namespace App\Console\Commands;

use App\Models\{Item, ItemImage, Category, Brand};
use Illuminate\Support\Facades\DB;
use DiDom\Document;

use Illuminate\Console\Command;

class DevCommannd extends Command
{
    const TMP_FILE = 'tmp.html';
    const BASE_URL = 'https://www.foxtrot.com.ua';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:com';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dev:com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->devParsing();
        $this->dispatchJob();
    }

    private function devParsing()
    {
        $doc = $this->openDocument();



        $item = $this->getBasicData($doc);
        // $item['url'] = $this->url;
        $brand = ['name' => $item['brand']];
        $item['brand_id'] = Brand::updateOrCreate($brand, $brand)->id;
        $item['category_id'] = $this->processCategories($doc);

        $breadCrumbs = $doc->find('.breadcrumbs ul li a[href]');
        $imagesNodes = $doc->find('.product-img__carousel img');

        $images = [];

        if (count($imagesNodes)) {

            foreach($imagesNodes as $img) {
                $link = trim($img->attr('src'));

                if ($link != '/') {
                    $images[] = $link;
                }
            }
        }

        print_r($item); print_r($images);

        return 0;
    }

    public function dispatchJob()
    {
        \App\Jobs\Tickets\ItemParserJob::dispatch("https://www.foxtrot.com.ua/ru/shop/vstraivaemue_holodilniki_whirlpool_sp40-801-eu.html");
    }

    private function downloadPage($url)
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => $url
        ]);

        $response = $client->request('GET');

        $code = $response->getStatusCode(); // 200

        if ($code != 200) {
            dd($output, $response);
        }

        $body = $response->getBody();

        // Explicitly cast the body to a string
        $stringBody = (string) $body;

        file_put_contents(
            self::TMP_FILE,
            $stringBody
        );
    }


    protected function getBasicData($doc)
    {

        $names = $doc->find('#product-page-title');

        $buyBtns = $doc->find('.product-box__main-buy__button');

        $pboxes = $doc->find('..product-box__right .product-box__content');


        // dd($breadCrumbs);

        if (count($names)) {
            $item['name'] = trim($names[0]->text());
        }

        if (count($buyBtns)) {
            $item['sku'] = trim($buyBtns[0]->attr('data-id'));
        }

        if (count($pboxes)) {
            $item['brand'] = trim($pboxes[0]->attr('data-brand'));
        }

        return $item;
    }

    protected function processCategories($doc)
    {
        $breadCrumbs = $doc->find('.breadcrumbs ul li a[href]');

        $categories = [];

        $categoryId = 0;

        if (count($breadCrumbs)) {

            foreach($breadCrumbs as $bc) {
                $link = trim($bc->attr('href'));

                if ($link != '/') {

                    $category = [
                        'name' => trim($bc->text()),
                        'url' => self::BASE_URL . $link
                    ];

                    if ($categoryId != 0) {
                        $category['parent_id'] = $categoryId;
                    }

                    $catModel = Category::updateOrCreate([
                        'url' => $category['url']
                    ], $category);

                    $categoryId = $catModel->id;
                }
            }
        }

        return $categoryId;
    }

    private function openDocument()
    {
        return new Document(file_get_contents(self::TMP_FILE));
    }

    private function isCategory($doc)
    {
        $nodes = $doc->find(".listing__body-products");

        if (empty($nodes)) {
            return false;
        }

        return true;

    }
}
