<?php

use App\User;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $roleAdmin = Role::where('name', 'admin')->first();
        // $roleUser  = Role::where('name', 'user')->first();

        $usersAdmins = [
            [
                'name' => 'Admin',
                'email' => 'admin@site.com',
                'password' => bcrypt('!bqChST%w9yvC8&h'),
            ]
        ];

        foreach ($usersAdmins as $key => $user) {

            $userAdmin = User::updateOrCreate([
                'email' => $user['email']
            ], $user);

            // if (!$userAdmin->hasRole('admin')) {
            //     $userAdmin->roles()->attach($roleAdmin);
            // }
        }


        // $usersUsers = [
        //     [
        //         'name' => 'User1',
        //         'email' => 'user1@site.com',
        //         'password' => bcrypt('G8cKL,xk8d&=7[bf'),
        //     ]
        // ];

        // foreach ($usersUsers as $key => $user) {

        //     $userUser = User::updateOrCreate([
        //         'email' => $user['email']
        //     ], $user);

        //     if (!$userUser->hasRole('user')) {
        //         $userUser->roles()->attach($roleUser);
        //     }
        // }
    }

}
