<?php

namespace App\Jobs\Tickets;

use App\Models\Categories;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class BaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const BASE_URL = 'https://www.foxtrot.com.ua';

    public $tries = 5;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }

    protected function downloadPage($url)
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => $url
        ]);

        $response = $client->request('GET');

        $code = $response->getStatusCode(); // 200

        if ($code != 200) {
            dd($output, $response);
        }

        $body = $response->getBody();

        return (string) $body;
    }

    private function isCategory($doc)
    {
        $nodes = $doc->find(".listing__body-products");

        if (empty($nodes)) {
            return false;
        }

        return true;

    }



}
