<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
    	'is_checked',
    	'is_category',
    	'is_processed',
    	'url'
    ];

    public function items()
    {
    	return $this->hasMany(Item::class);
    }
}
