@extends('layouts.app')

@section('content')

    @include('tickets.new-request')

    <br>
    <br>

    <div class="row">
        <div class="col-md-6">
            <h3>Запити ({{ $count }} шт.)</h3>
        </div>

    </div>

    <br>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Перевірено</th>
                <th scope="col" style="display:none">Опрацьовано</th>
                <th scope="col">Категорія</th>
                <th scope="col">URL</th>
                <th scope="col">Кількість товарів</th>
            </tr>
        </thead>
        <tbody>


            @foreach($items as $item)

            <tr class="align-middle">

                <td>
                    {{ $item->id }}
                </td>

                <td>
                    {{ $item->is_checked ? 'так' : 'ні' }}
                </td>


                <td style="display:none">
                    {{ $item->is_processed ? 'так' : 'ні' }}
                </td>

                <td>
                    {{ $item->is_category ? 'так' : 'ні' }}
                </td>

                <td>
                    {{ $item->url }}
                </td>

                <td>
                    {{ $item->items()->count() }}
                </td>


            </tr>

            @endforeach

        </tbody>
    </table>

    <br>

    <div class="pagination justify-content-center">
        {!! $items->appends(\Request::except('page'))->render() !!}
    </div>

@endsection


@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css" integrity="sha512-oe8OpYjBaDWPt2VmSFR+qYOdnTjeV9QPLJUeqZyprDEQvQLJ9C5PCFclxwNuvb/GQgQngdCXzKSFltuHD3eCxA==" crossorigin="anonymous" />
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous"></script>
@endpush
