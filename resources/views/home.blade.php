@extends('layouts.app')

@section('content')


    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="btn btn-success btn-block" id="btn"> Button "A" </div>
        </div>
    </div>

    <br><br>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="jumbotron" id="block">
                Block "B"
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript">

        window.myUrl = 'https://nowdialogue.com/api/merchant/48/widget/presets/86';

        window.onload = function() {
            console.log('onload');

            let btn = document.getElementById('btn');
            let block = document.getElementById('block');

            console.log(btn, block);

            btn.onclick = function() {
                loadDoc(window.myUrl, function(result){
                    console.log(result);

                    block.innerHTML = result;

                    fetchAndEvalScripts();
                });
            };

        };


        function fetchAndEvalScripts(result) {

            let scripts = block.getElementsByTagName('script');

            for(var i in scripts) {
                if (scripts.hasOwnProperty(i)) {
                    let script = scripts[i].innerText.replaceAll('\n','');
                    eval(script);
                }
            }

        }

        function loadDoc(url, callback) {

            let xmlhttp = new XMLHttpRequest();

            xmlhttp.responseType = 'json';

            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
                   if (xmlhttp.status == 200) {
                        console.log(xmlhttp.response);


                        callback(xmlhttp.response);
                       // document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
                   }
                   else if (xmlhttp.status == 400) {
                      alert('There was an error 400');
                   }
                   else {
                       alert('something else other than 200 was returned');
                   }
                }
            };

            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }

    </script>
@endpush
