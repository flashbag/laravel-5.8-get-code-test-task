<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Зробити новий запит</h3>
            </div>

            <div class="card-body">

            	<div class="row">
            		<div class="col-md-12">
            			<h4>Введіть посилання</h4>
            		</div>
            	</div>


            	<div class="row">
            		<div class="col-md-12">
            			<input type="url" value="https://www.foxtrot.com.ua/uk/shop/vstraivaemue_holodilniki.html" id="url" name="url" class="form-control">
            		</div>
            	</div>

            	<br>

            	<div class="row">
            		<div class="col-md-12">
            			<button type="submit" id="btn-submit" class="btn btn-success">Відправити</button>
            		</div>
            	</div>

            </div>

        </div>

    </div>
</div>

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function(){

			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

			function storeTicket() {

				let $url = $('#url');

				let url = $url.val();

				if (!url) {
					return false;
				}

				$.ajax({
		            url: '/tickets',
		            method: 'POST',
		            dataType: 'json',
		            data: {
		            	url: url
		            },
		            success: function (response) {
		            	console.log(response);
		                window.location.href = response.redirect;
		            },
		            error: function (response) {
		                // alert('Error!');
		                console.error(response);
		            }
		        });

			}

			$('#btn-submit').on('click', function(e){
				e.preventDefault();

				storeTicket();
			})
		});
	</script>
@endpush