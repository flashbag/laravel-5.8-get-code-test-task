@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-6">
            <h3>Товари ({{ $count }} шт.)</h3>
        </div>

    </div>

    <br>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">SKU</th>
                <th scope="col">Назва</th>
                <th scope="col">URL</th>
            </tr>
        </thead>
        <tbody>


            @foreach($items as $item)

            <tr class="align-middle type-primary"
                data-type="primary"
                data-id="{{ $item->id }}"
                style="cursor: pointer; ">

                <td>
                    {{ $item->id }}
                </td>

                 <td>
                    {{ $item->sku }}
                </td>

                <td>
                    {{ $item->name }}
                </td>


                <td>
                    {{ $item->url }}
                </td>

            </tr>


            <tr class="align-middle type-secondary"
                data-type="secondary"
                data-id="{{ $item->id }}"
                style="display:none"
                >

                <th colspan="3" class="text-center">
                    Дерево категорій
                </th>
                <th colspan="3" class="text-center">
                    Посилання на фото
                </th>
            </tr>

            <tr class="align-middle type-secondary"
                data-type="secondary"
                data-id="{{ $item->id }}"
                style="display:none"
                >

                <th colspan="3" class="text-center">
                    @foreach($item->categoriesTree as $category)
                        <a href="{{ $category->url }}" target="_blank">
                            {{ $category->name }}
                        </a>
                        <br>
                    @endforeach
                </th>

                <th colspan="3" class="text-center">

                    @foreach($item->images as $image)

                        <a href="{{ $image->url }}" target="_blank">
                            {{ $image->url }}
                        </a>
                        <br>

                    @endforeach
                </th>
            </tr>

            @endforeach

        </tbody>
    </table>

    <br>

    <div class="pagination justify-content-center">
        {!! $items->appends(\Request::except('page'))->render() !!}
    </div>

@endsection


@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){

            $('tr.type-primary').on('click', function(e){

                e.preventDefault();

                let $row = $(this);

                $('tr.type-secondary').hide();
                $('tr.type-secondary').hide();
                $('tr.type-secondary').hide();

                let id = $row.data('id');
                let selector = 'tr[data-id="' + id + '"].type-secondary';

                if (!$row.hasClass('active')) {

                    $('tr.type-primary').removeClass('active');
                    $row.addClass('active');

                    $(selector).show();

                    console.log(selector);
                    console.log($(selector));

                } else {

                    $('tr.type-primary').removeClass('active');

                    $(selector).hide();
                }


            });
        });
    </script>
@endpush