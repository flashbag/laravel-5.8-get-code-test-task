@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Запит #{{ $ticket->id }}</h3>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6">
                            <h5>Перевірено</h5>
                        </div>

                        <div class="col-md-6 text-right">
                            <h5>{{ $ticket->is_checked ? 'так' : 'ні' }}</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>Опрацьовано</h5>
                        </div>

                        <div class="col-md-6 text-right">
                            <h5>{{ $ticket->is_processed ? 'так' : 'ні' }}</h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5>Категорія</h5>
                        </div>

                        <div class="col-md-6 text-right">
                            <h5>{{ $ticket->is_category ? 'так' : 'ні' }}</h5>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6">
                            <h5>URL</h5>
                        </div>

                        <div class="col-md-6 text-right">
                            <h5>{{ $ticket->url }}</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <br>
    <br>

    <div class="row">
        <div class="col-md-6">
            <h3>Товари ({{ 0 }} шт.)</h3>
        </div>

    </div>

    <br>

    <table class="table">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">SKU</th>
                <th scope="col">Назва товару</th>
                <th scope="col">Бренд</th>
                <th scope="col">Категорії</th>
                <th scope="col">URL</th>

            </tr>
        </thead>
        <tbody>


            @foreach($ticket->items as $item)

            <tr class="align-middle">

                <td>
                    {{  }}
                </td>

            </tr>

            @endforeach

        </tbody>
    </table>

    <br>

@endsection

