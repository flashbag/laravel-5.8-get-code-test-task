<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    protected $fillable = [
    	'url',
    	'sku',
    	'name',

        'brand_id',
    	'ticket_id',
        'category_id'
    ];


    public static function getSelectors()
    {
    	return [
    		'name' => '.product-page-title',
    	];
    }

    public function ticket()
    {
    	return $this->belongsTo(Ticket::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function images()
    {
        return $this->hasMany(ItemImage::class);
    }


    public function getCategoriesTreeAttribute()
    {
        $id = $this->category_id;

        $query = DB::select("
            WITH RECURSIVE category_path (id, name, url, parent_id) AS
            (
              SELECT id, name, url, parent_id
                FROM categories
                WHERE id = ${id}
              UNION ALL
              SELECT c.id, c.name, c.url, c.parent_id
                FROM category_path AS cp JOIN categories AS c
                  ON cp.parent_id = c.id
            )
            SELECT * FROM category_path;
        ");

        return array_reverse((array)$query);
    }
}
