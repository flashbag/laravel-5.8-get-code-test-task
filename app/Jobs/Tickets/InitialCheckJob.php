<?php

namespace App\Jobs\Tickets;

use App\Models\Ticket;
use DiDom\Document;

class InitialCheckJob extends BaseJob
{
    protected $ticket;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @param  Podcast  $podcast
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $html = $this->downloadPage($this->ticket->url);

        // file_put_contents('tmp.html', $html);

        $doc = new Document($html);

        $isCategory = $this->isCategory($doc);

        echo "ticket # " . $this->ticket->id . PHP_EOL;
        echo "isCategory: " . $isCategory . PHP_EOL;

        $this->ticket->update([
            'is_category' => $isCategory,
            'is_checked' => true
        ]);

        if ($isCategory) {
            // fetch links, and create ItemParserJob jobs
            foreach($doc->find('.card__body .card__title') as $link) {
                $url = self::BASE_URL . $link->attr('href');
                echo "url:" . $url . PHP_EOL;
                ItemParserJob::dispatch($this->ticket, $url);
            }

        } else {
            // create ItemParserJob job on $ticket->url
            ItemParserJob::dispatch($this->ticket, $this->ticket->url);
        }

        return 0;
    }

    private function isCategory($doc)
    {
        $nodes = $doc->find(".listing__body-products");

        if (empty($nodes)) {
            return false;
        }

        return true;

    }
}
