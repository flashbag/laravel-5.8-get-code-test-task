<?php

namespace App\Jobs\Tickets;

use App\Models\{Ticket, Item, ItemImage, Brand, Category};
use Illuminate\Support\Facades\DB;
use DiDom\Document;

class ItemParserJob extends BaseJob
{
    public $tries = 5;

    protected $ticket;

    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, string $url)
    {
        $this->url = $url;
        $this->ticket = $ticket;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "ItemParserJob handle" . PHP_EOL;
        echo '$this->url:' . $this->url . PHP_EOL;

        $html = $this->downloadPage($this->url);

        $item = [
            'url' => $this->url,
        ];

        $doc = new Document($html);

        $imagesNodes = $doc->find('.product-img__carousel img');

        // dd($breadCrumbs);

        $item = $this->getBasicData($doc);
        $item['url'] = $this->url;
        $item['ticket_id'] = $this->ticket->id;
        $brand = ['name' => $item['brand']];
        $item['brand_id'] = Brand::updateOrCreate($brand, $brand)->id;
        $item['category_id'] = $this->processCategories($doc);

        $images = [];

        if (count($imagesNodes)) {

            foreach($imagesNodes as $img) {
                $link = trim($img->attr('src'));

                if ($link != '/') {
                    $images[] = $link;
                }
            }

            $images;
        }

        print_r($item);

        DB::beginTransaction();

        $itemObj = Item::updateOrCreate(['url' => $item['url']], $item);

        foreach($images as $url) {
            $itemObj->images()->create([
                'item_id'=>$itemObj->id,
                'url' => $url
            ]);
        }

        DB::commit();

        return 0;
    }

    protected function getBasicData($doc)
    {

        $item = [];

        $names = $doc->find('#product-page-title');

        $buyBtns = $doc->find('.product-box__main-buy__button');

        $pboxes = $doc->find('..product-box__right .product-box__content');


        // dd($breadCrumbs);

        if (count($names)) {
            $item['name'] = trim($names[0]->text());
        }

        if (count($buyBtns)) {
            $item['sku'] = trim($buyBtns[0]->attr('data-id'));
        }

        if (count($pboxes)) {
            $item['brand'] = trim($pboxes[0]->attr('data-brand'));
        }

        return $item;
    }

    protected function processCategories($doc)
    {
        $breadCrumbs = $doc->find('.breadcrumbs ul li a[href]');

        $categories = [];

        $categoryId = 0;

        if (count($breadCrumbs)) {

            foreach($breadCrumbs as $bc) {
                $link = trim($bc->attr('href'));

                var_dump($link);

                if ($link != '/' && $link != '/uk') {

                    $category = [
                        'name' => trim($bc->text()),
                        'url' => self::BASE_URL . $link
                    ];

                    if ($categoryId != 0) {
                        $category['parent_id'] = $categoryId;
                    }

                    $catModel = Category::updateOrCreate([
                        'url' => $category['url']
                    ], $category);

                    $categoryId = $catModel->id;
                }
            }
        }

        return $categoryId;
    }

    protected function processImages($doc)
    {
        $imagesNodes = $doc->find('.product-img__carousel img');

        $images = [];

        if (count($imagesNodes)) {

            foreach($imagesNodes as $img) {
                $link = trim($img->attr('src'));

                if ($link != '/') {
                    $images[] = $link;
                }
            }

            $item['images'] = $images;
        }
    }
}
